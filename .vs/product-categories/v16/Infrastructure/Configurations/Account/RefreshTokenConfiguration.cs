﻿using Domain.Models.Account;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Configurations.Account
{
    public class RefreshTokenConfiguration : IEntityTypeConfiguration<RefreshTokenEntity>
    {
        public void Configure(EntityTypeBuilder<RefreshTokenEntity> builder)
        {


            builder.HasIndex(rt => rt.JwtId)
                .HasDatabaseName("RefreshTokenJwtIdIndex")
                .IsUnique();


            builder.ToTable("RefreshTokens");
        }
    }
}
