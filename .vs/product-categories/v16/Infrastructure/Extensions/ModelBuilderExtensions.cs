﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using Domain.Constants;
using Domain.Models.Account;

namespace Infrastructure.Extensions
{
    public static class ModelBuilderExtensions
    {
        private const string SuperAdminId = "01b168fe-810b-432d-9010-233ba0b380e9";
        private const string RoleUserId = "a18be9c0-aa65-4af8-bd17-00bd9344e575";
        private const string RoleAdminId = "2301d884-221a-4e7d-b509-0113dcc043E1";

        public static void SeedRoles(this ModelBuilder modelBuilder)
        {
            var roleUser = new RoleEntity
            {
                Id = RoleUserId,
                Name = Roles.User.ToString(),
                NormalizedName = Roles.User.ToString().ToUpper(),
                CreatedAt = DateTime.UtcNow
            };
            var roleAdmin = new RoleEntity
            {
                Id = RoleAdminId,
                Name = Roles.Admin.ToString(),
                NormalizedName = Roles.Admin.ToString().ToUpper(),
                CreatedAt = DateTime.UtcNow
            };


            modelBuilder.Entity<RoleEntity>()
                .HasData(roleUser, roleAdmin);
        }

        public static void SeedUsers(this ModelBuilder modelBuilder)
        {
            var userName = "admin";
            var email = "admin@admin.com";
            var superAdmin = new UserEntity
            {
                Id = SuperAdminId,
                FirstName = "Super",
                LastName = "Admin",
                UserName = userName,
                NormalizedUserName = userName.ToUpper(),
                Email = email,
                NormalizedEmail = email.ToUpper(),
                TwoFactorEnabled = false,
                EmailConfirmed = true,
                PhoneNumber = "000-000-0000",
                PhoneNumberConfirmed = true
            };

            PasswordHasher<UserEntity> ph = new PasswordHasher<UserEntity>();
            superAdmin.PasswordHash = ph.HashPassword(superAdmin, "123a123a");

            modelBuilder.Entity<UserEntity>()
                .HasData(superAdmin);
        }

        public static void SeedUserRoles(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserRoleEntity>()
                .HasData(
                    new UserRoleEntity() {UserId = SuperAdminId, RoleId = RoleUserId},
                    new UserRoleEntity() {UserId = SuperAdminId, RoleId = RoleAdminId});
        }

    }
}