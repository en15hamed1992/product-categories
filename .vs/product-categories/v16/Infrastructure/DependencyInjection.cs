﻿using System;
using Application.Interfaces;
using Application.Services;
using Domain.Constants;
using Domain.Interfaces;
using Domain.Models.Account;
using Infrastructure.Data;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure
                      (this IServiceCollection services,
                       IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString(
                    "DefaultConnection")));

            services.AddIdentity<UserEntity, RoleEntity>(ConstantsVariabels.IdentityOptions)
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(ConstantsVariabels.AuthenticationOptions)
                .AddJwtBearer(ConstantsVariabels.JwtBearerOptions);


            services.AddAuthorizationCore(options =>
            {
                var defaultAuthBuilder = new AuthorizationPolicyBuilder();
                options.DefaultPolicy = defaultAuthBuilder
                    .RequireAuthenticatedUser()
                    .RequireClaim("Id")
                    .Build();
            });
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            return services;
        }
    }
}
