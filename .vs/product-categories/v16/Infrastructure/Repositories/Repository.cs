﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Interfaces;
using Infrastructure.Data;
using Infrastructure.Extensions;

namespace Infrastructure.Repositories
{
    public class Repository<TEntity, TId> : IRepository<TEntity,TId>
        where TEntity : class,IEntity<TId>
        where TId : IComparable, IConvertible, IEquatable<TId>
    {
        private readonly ApplicationDbContext _context;
        private readonly DbSet<TEntity> _entities;

        public Repository(ApplicationDbContext context)
        {
            _context = context;
            _entities = context.Set<TEntity>();
        }

        public async Task<IEnumerable<TEntity>> FindAllAsync(ISpecification<TEntity> specification, int page,
            byte pageSize=10)
        {
            var query = _entities.ApplySpecification(specification).AsQueryable();

            return await query.AsNoTracking().Skip(page*pageSize).Take(pageSize).ToListAsync();
        }

        public async Task<IEnumerable<TResult>> FindAllAsync<TResult>(ISpecification<TEntity> specification,
            IProjectSpecification<TEntity, TResult> projectSpecification, int page,
            byte pageSize)
        {
            var query = _entities.ApplySpecification(specification)
                .Project(projectSpecification)
                .AsQueryable();

            return await query.Skip(page*pageSize).Take(pageSize).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> FindAllAsync(ISpecification<TEntity> specification)
        {
            return await _entities.ApplySpecification(specification).ToListAsync();
        }

        public async Task<TEntity> FindAsync(ISpecification<TEntity> specification)
        {
            return await _entities.ApplySpecification(specification).FirstOrDefaultAsync();
        }

        public async Task<TResult> FindAsync<TResult>(ISpecification<TEntity> specification,
            IProjectSpecification<TEntity, TResult> projectSpecification)
        {
            return await _entities.ApplySpecification(specification).Project(projectSpecification).FirstOrDefaultAsync();
        }

        public void Create(TEntity entity) => _context.Add(entity);

        public void Remove(TEntity entity) => _context.Remove(entity);
    }
}