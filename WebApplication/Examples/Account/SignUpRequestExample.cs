﻿using Application.DTOs.Account;
using Swashbuckle.AspNetCore.Filters;

namespace API.Examples.Account
{
    public class SignUpRequestExample : IExamplesProvider<SignUpDto>
    {
        public SignUpDto GetExamples()
        {
            return new SignUpDto()
            {
                
                Email = "example@example.com",
                PhoneNumber = "123123123",
                
                FirstName = "John",
                LastName = "Doe",
                Password = "Lamaz123@",
                ConfirmPassword = "Lamaz123@"
            };
        }
    }
}
