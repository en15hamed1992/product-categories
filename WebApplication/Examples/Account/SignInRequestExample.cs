﻿using Application.DTOs.Account;
using Swashbuckle.AspNetCore.Filters;

namespace API.Examples.Account
{
    public class SignInRequestExample : IExamplesProvider<SignInDto>
    {
        public SignInDto GetExamples()
        {
            return new SignInDto()
            {
                Email = "admin@admin.com",
              Password = "Admin1@"
            };
        }
    }
}
