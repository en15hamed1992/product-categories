﻿using Application.DTOs.Account;
using Swashbuckle.AspNetCore.Filters;

namespace API.Examples.Account
{
    public class RefreshTokenRequestExample : IExamplesProvider<RefreshTokenDto>
    {
        public RefreshTokenDto GetExamples()
        {
            return new RefreshTokenDto()
            {
                RefreshToken = "Refresh token",
                AccessToken = "Access token"
            };
        }
    }
}
