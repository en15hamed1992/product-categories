﻿using System;
using System.Linq.Expressions;

namespace Domain.Interfaces
{
    public interface IProjectSpecification<TEntity, TResult>
    {
        Expression<Func<TEntity, TResult>> Projection { get; }

        void ApplyProjection(Expression<Func<TEntity, TResult>> projectExpression);
    }
}
