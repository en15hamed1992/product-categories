﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Domain.Interfaces
{
    public interface ISpecification<T>
    {
        Expression<Func<T, bool>> Criteria { get; }

        List<Expression<Func<T, object>>> Includes { get; }
        List<string> IncludeStrings { get; }

        List<Expression<Func<T, bool>>> Filters { get; }

        Expression<Func<T, object>> OrderBy { get; }
        Expression<Func<T, object>> OrderByDescending { get; }

        Expression<Func<T, object>> ThenBy { get; }
        Expression<Func<T, object>> ThenByDescending { get; }

        Expression<Func<T, object>> GroupBy { get; }

        bool IgnoreQueryFilters { get; }

        void AddInclude(Expression<Func<T, object>> includeExpression);

        void AddInclude(string includeString);

        void AddFilter(Expression<Func<T, bool>> filterExpression);

        void ApplyOrderBy(Expression<Func<T, object>> orderByExpression);

        void ApplyOrderByDescending(Expression<Func<T, object>> orderByDescendingExpression);

        void ApplyThenBy(Expression<Func<T, object>> thenByExpression);

        void ApplyThenByDescending(Expression<Func<T, object>> thenByDescendingExpression);

        void ApplyGroupBy(Expression<Func<T, object>> groupByExpression);

        void ApplyIgnoreQueryFilters();

    }
}
