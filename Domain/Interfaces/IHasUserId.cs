﻿namespace Domain.Interfaces
{
    public interface IHasUserId
    {
        public string UserId { get; set; }
    }
}