﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IRepository<TEntity, in TId>
       where TEntity : class, IEntity<TId>
       where TId : IComparable, IConvertible, IEquatable<TId>
    {
        Task<IEnumerable<TEntity>> FindAllAsync(ISpecification<TEntity> specification, int page, byte pageSize);

        Task<IEnumerable<TResult>> FindAllAsync<TResult>(ISpecification<TEntity> specification,
            IProjectSpecification<TEntity, TResult> projectSpecification, int page, byte pageSize);

        Task<IEnumerable<TEntity>> FindAllAsync(ISpecification<TEntity> specification);

        Task<TEntity> FindAsync(ISpecification<TEntity> specification);

        Task<TResult> FindAsync<TResult>(ISpecification<TEntity> specification,
            IProjectSpecification<TEntity, TResult> projectSpecification);

        void Create(TEntity competition);

        void Remove(TEntity competition);
    }
}
