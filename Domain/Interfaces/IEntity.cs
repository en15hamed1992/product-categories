﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain.Interfaces
{
    public interface IEntity<TId>
        where TId : IComparable, IConvertible, IEquatable<TId>
    {
        [Key]
        public TId Id { get; set; }

        public DateTime CreatedAt { get; init; }

        public DateTime UpdatedAt { get; set; }

    }
}
