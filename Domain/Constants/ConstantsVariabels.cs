﻿using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication;

namespace Domain.Constants
{
    public static class ConstantsVariabels
    {
        private const string _authKey = "ThisKeyShouldBeSecret";
        private static TokenValidationParameters TokenValidationParameters => new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_authKey)),
            ValidateIssuer = false,
            ValidateAudience = false,
            RequireExpirationTime = false,
            ValidateLifetime = true,
            ClockSkew = TimeSpan.Zero
        };

        public static Action<IdentityOptions> IdentityOptions => (options =>
       {
           options.Password.RequireDigit = true;
           options.Password.RequireLowercase = true;
           options.Password.RequiredLength = 4;
           options.Password.RequireUppercase = true;
           options.SignIn.RequireConfirmedPhoneNumber = true;
       });

        public static Action<AuthenticationOptions> AuthenticationOptions => (options =>
          {
              options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
              options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
          });

        public static Action<JwtBearerOptions> JwtBearerOptions => (options =>
        {
            options.RequireHttpsMetadata = false;
            options.SaveToken = true;
            options.TokenValidationParameters = TokenValidationParameters;


        });
    }
}
