﻿using Domain.Interfaces;
using System;


namespace Domain.Models
{
    public class BaseEntity : IEntity<int>
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; init; }

        public DateTime UpdatedAt { get; set; }

        public BaseEntity()
        {
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }

    }
}
