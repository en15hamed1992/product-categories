﻿using Microsoft.AspNetCore.Identity;

namespace Domain.Models.Account
{
    public class UserClaimEntity : IdentityUserClaim<string>
    {
        public UserEntity User { get; set; }
    }
}
