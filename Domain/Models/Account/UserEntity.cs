﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace Domain.Models.Account
{
    public class UserEntity : IdentityUser, IEntity<string>
    {
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        public string NormalizedFullName { get; set; }


        public DateTime CreatedAt { get; init; }

        public DateTime UpdatedAt { get; set; }

        public ICollection<UserRoleEntity> UserRoles { get; set; }

        public ICollection<UserClaimEntity> Claims { get; set; }

        public ICollection<RefreshTokenEntity> Tokens { get; set; }



        public UserEntity()
        {
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
            UserRoles = new List<UserRoleEntity>();
            Claims = new List<UserClaimEntity>();
            Tokens = new List<RefreshTokenEntity>();

        }
    }
}
