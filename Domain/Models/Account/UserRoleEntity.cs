﻿using Microsoft.AspNetCore.Identity;

namespace Domain.Models.Account
{
    public class UserRoleEntity : IdentityUserRole<string>
    {
        public UserEntity User { get; set; }

        public RoleEntity Role { get; set; }
    }
}
