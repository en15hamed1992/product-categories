﻿using Domain.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Domain.Models.Account
{
    public class RoleEntity : IdentityRole, IEntity<string>
    {
        public ICollection<UserRoleEntity> UserRoles { get; set; }


        public DateTime CreatedAt { get; init; }

        public DateTime UpdatedAt { get; set; }


        public RoleEntity()
        {
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
            UserRoles = new List<UserRoleEntity>();
        }
    }
}
