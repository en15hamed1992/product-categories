﻿using Domain.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models.Account
{
    public class RefreshTokenEntity : IEntity<int>,IHasUserId
    {
        public int Id { get; set; }

        [Required]
        public string JwtId { get; set; }
        [Required] 
        public virtual string Token { get; set; }

        public string UserId { get; set; }

        public UserEntity User { get; set; }
        
        public DateTime CreatedAt { get; init; }

        public DateTime UpdatedAt { get; set; }


        public RefreshTokenEntity()
        {
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }
    }
}
