﻿using Domain.Models.Account;

namespace Domain.Specifications.User
{
    public class UserSpecification:BaseSpecification<UserEntity>
    {
        public UserSpecification()
        {
                
        }

        public UserSpecification(string id)
        :base(e=>e.Id==id)
        {
            
        }
    }
}