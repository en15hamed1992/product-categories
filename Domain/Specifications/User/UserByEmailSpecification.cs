﻿using Domain.Models.Account;

namespace Domain.Specifications.User
{
    public class UserByEmailSpecification:BaseSpecification<UserEntity>
    {
        public UserByEmailSpecification(string email)
        :base(entity=>entity.Email==email)
        {
                
        }
    }
}