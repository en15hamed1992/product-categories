﻿using Domain.Models.Account;

namespace Domain.Specifications.User
{
    public class UserWithClaims :BaseSpecification<UserEntity>
    {
        public UserWithClaims()
        {
                AddInclude(e=>e.Claims);
        }

        
        
    }
}