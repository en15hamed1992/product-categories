﻿using Domain.Interfaces;

namespace Domain.Specifications.User
{
    public class EntityByUserIdSpecification<T>:BaseSpecification<T>
       where T:IHasUserId
    {
        public EntityByUserIdSpecification(string userId)
        :base(entity=>entity.UserId==userId)
        {
           
        }
    }
}