﻿using Domain.Models.Account;

namespace Domain.Specifications.Role
{
    public class RoleSpecification:BaseSpecification<RoleEntity>
    {
        public RoleSpecification()
        {
            
        }

        public RoleSpecification(string id)
        :base(e=>e.Id==id)
        {
            
        }
    }
}