using System;
using System.Linq.Expressions;
using Domain.Interfaces;

namespace Domain.Specifications
{
    public class SpecificationBuilder<T>
    {
        private readonly ISpecification<T> _specification;

        public SpecificationBuilder(ISpecification<T> specification)
        {
            _specification = specification;
        }

        public SpecificationBuilder<T> Append(ISpecification<T> specification)
        {
            if (specification.Criteria != null)
            {
                _specification.AddFilter(specification.Criteria);
            }

            specification.Includes.ForEach(_specification.AddInclude);
            specification.IncludeStrings.ForEach(_specification.AddInclude);

            specification.Filters.ForEach(_specification.AddFilter);

            _specification.ApplyOrderBy(specification.OrderBy);
            _specification.ApplyOrderByDescending(specification.OrderByDescending);
            _specification.ApplyThenBy(specification.ThenBy);
            _specification.ApplyThenByDescending(specification.ThenByDescending);

            if (specification.GroupBy != null)
            {
                _specification.ApplyGroupBy(specification.GroupBy);
            }

            if (specification.IgnoreQueryFilters)
            {
                _specification.ApplyIgnoreQueryFilters();
            }

            return this;
        }

        public SpecificationBuilder<T> Criteria(Expression<Func<T, bool>> criteria)
        {
            _specification.AddFilter(criteria);

            return this;
        }

        public ISpecification<T> Build()
        {
            return _specification;
        }
    }
}