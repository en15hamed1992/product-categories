﻿using Domain.Models.Account;

namespace Domain.Specifications.RefreshToken
{
    public class RefreshTokenSpecification:BaseSpecification<RefreshTokenEntity>
    {
        public RefreshTokenSpecification( string token)
        :base(e=>e.Token==token)
        {
                AddInclude(e=>e.User);
        }
    }
}