﻿namespace Domain.Specifications
{
    public abstract class BaseSpecificationDecorator<T> : BaseSpecification<T>
    {
        protected readonly BaseSpecification<T> _specification;

        protected BaseSpecificationDecorator(BaseSpecification<T> specification)
            : base(specification.Criteria)
        {
            specification.Includes.ForEach(include => AddInclude(include));
            specification.IncludeStrings.ForEach(include => AddInclude(include));
            specification.Filters.ForEach(filter => AddFilter(filter));

            ApplyOrderBy(specification.OrderBy);
            ApplyOrderByDescending(specification.OrderByDescending);
            ApplyThenBy(specification.ThenBy);
            ApplyThenByDescending(specification.ThenByDescending);

            _specification = specification;
        }

    }
}
