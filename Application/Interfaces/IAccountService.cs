﻿using System.Threading.Tasks;
using Application.DTOs.Account;
using Microsoft.AspNetCore.Mvc;

namespace Application.Interfaces
{
    public interface IAccountService
    {
        Task<IActionResult> SignUpAsync(SignUpDto signUpDto);

        Task<IActionResult> SignInAsync(SignInDto signInDto);
      
        
        Task<IActionResult> RefreshTokenAsync(RefreshTokenDto refreshDto);

        
    }
}
