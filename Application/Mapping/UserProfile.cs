﻿using AutoMapper;
using System.Linq;
using Application.DTOs.User;
using Application.Handlers;
using Domain.Models.Account;

namespace Application.Mapping
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            // Map domain model to services DTO
            CreateMap<UserEntity, UserDto>()
                .ForMember(dto => dto.Roles, opt => 
                    opt.MapFrom(entity =>
                entity.UserRoles.Select(ur => new RoleDto()
                {
                    Id = ur.Role.Id,
                    Name = ur.Role.Name
                })))
                .ForMember(dto => dto.CreatedAt, opt =>
                    opt.MapFrom(entity => entity.CreatedAt.ToDateTimeString()))
                .IncludeAllDerived();

            CreateMap<RoleEntity, RoleDto>();

            CreateMap<RoleDto, RoleEntity>();

        }
    }
}
