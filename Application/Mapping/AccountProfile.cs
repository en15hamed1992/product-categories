﻿using AutoMapper;
using System;
using Application.DTOs.Account;
using Domain.Models.Account;

namespace Application.Mapping
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            // Map core layer DTO to domain models
            CreateMap<SignUpDto, UserEntity>()
                .ForMember(entity => entity.UserName, 
                    opt => 
                        opt.MapFrom(dto => Guid.NewGuid().ToString()))
                .ForMember(entity => entity.NormalizedFullName,
                    opt => 
                        opt.MapFrom(dto => dto.FirstName + " " + dto.LastName))
                .IncludeAllDerived();

            // Map domain models to core layer DTO
            CreateMap<UserEntity, SignUpResultDto>()
                .ForMember(dto => dto.Name, opt => opt.MapFrom(entity => entity.FirstName + " " + entity.LastName))
                .ForMember(dto => dto.EmailOrPhone, opt => opt.MapFrom(entity => entity.Email ?? entity.PhoneNumber));
        }
    }
}
