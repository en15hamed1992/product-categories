﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Application.DTOs.Account;
using Application.Handlers;
using Application.Interfaces;
using AutoMapper;
using Domain.Constants;
using Domain.Interfaces;
using Domain.Models.Account;
using Domain.Specifications;
using Domain.Specifications.RefreshToken;
using Domain.Specifications.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TokenHandler = Application.Handlers.TokenHandler;

namespace Application.Services
{
    public class AccountService : BaseService, IAccountService
    {
        private readonly UserManager<UserEntity> _userManager;
        private readonly IRepository<UserEntity, string> _userRepository;
        private readonly IRepository<RefreshTokenEntity, int> _refreshTokenEntity;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;


        public AccountService(IHttpContextAccessor httpContextAccessor,
            UserManager<UserEntity> userManager,
            IRepository<UserEntity, string> userRepository,
            IRepository<RefreshTokenEntity, int> refreshTokenEntity,
            IUnitOfWork unitOfWork,
            IMapper mapper)
            : base(httpContextAccessor)
        {
            _userManager = userManager;
            _userRepository = userRepository;
            _refreshTokenEntity = refreshTokenEntity;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IActionResult> SignUpAsync(SignUpDto signUpDto)
        {
            try
            {
                var user = _mapper.Map<SignUpDto, UserEntity>(signUpDto);
                user.EmailConfirmed = true;
                user.PhoneNumberConfirmed = true;
                user.SecurityStamp = Guid.NewGuid().ToString();
                var createdUser = await _userManager.CreateAsync(user);
                if (!createdUser.Succeeded)
                    return BadRequest($"Cant Create User",createdUser.Errors);
                
                _userManager.AddPasswordAsync(user, signUpDto.Password).Wait();

                _userManager.AddToRoleAsync(user, Roles.User.ToString()).Wait();
                _userManager.AddClaimsAsync(user, new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, user.Id),
                    new Claim(ClaimTypes.Role, Roles.User.ToString()),
                    new Claim(ClaimTypes.MobilePhone, user.PhoneNumber),
                    new Claim(JwtRegisteredClaimNames.Jti, new Guid().ToString())
                }).Wait();
                return Ok("User Has Been Created Successfully");
            }
            catch (Exception e)
            {
               return e.Handel();
            }
           
          
        }

        public async Task<IActionResult> SignInAsync(SignInDto signInDto)
        {
            try
            {
                var specifications = new SpecificationBuilder<UserEntity>
                        (new UserByEmailSpecification(signInDto.Email))
                    .Append(new UserWithClaims())
                    .Build();
                var user = await _userRepository.FindAsync(specifications);
                if (user is null) return NotFound("User Not Found");

                if (!_userManager.CheckPasswordAsync(user, signInDto.Password).Result)
                    return BadRequest("Invalid user Name or password");

                var storedToken =await _refreshTokenEntity
                        .FindAsync(new EntityByUserIdSpecification<RefreshTokenEntity>(user.Id));

                if (storedToken is null)
                {
                   _refreshTokenEntity.Create(new RefreshTokenEntity()
                    {
                        JwtId =Guid.NewGuid().ToString(),
                        UserId = user.Id,
                        Token = await TokenHandler.GenerateRefreshToken()
                      
                    });
                   await _unitOfWork.CompleteAsync();
                }
                else
                {
                    storedToken.Token = await TokenHandler.GenerateRefreshToken();
                    await _unitOfWork.CompleteAsync();
                }

                return Create($"Welcome {user.FirstName} {user.LastName}", new RefreshTokenDto()
                {
                    AccessToken = await TokenHandler.GenerateJwtToken(_userManager,user),
                    RefreshToken = storedToken?.Token
                });
            }
            catch (Exception e)
            {
              return  e.Handel();
            }
        }

       

      

        public async Task<IActionResult> RefreshTokenAsync(RefreshTokenDto refreshDto)
        {
            try
            {
                var refreshToken =
                    await _refreshTokenEntity.FindAsync(new RefreshTokenSpecification(refreshDto.RefreshToken));
                if (refreshToken is null) return BadRequest("Invalid Tokens");

                refreshToken.Token = await TokenHandler.GenerateRefreshToken();
                _unitOfWork.CompleteAsync().Wait();

                return Ok("Refreshed Successfully",
                    new RefreshTokenDto()
                    {
                        AccessToken = await TokenHandler.GenerateJwtToken(_userManager,refreshToken.User),
                        RefreshToken = refreshToken.Token
                    });
            }
            catch (Exception e)
            {
               return e.Handel();
            }
        }

    }
}