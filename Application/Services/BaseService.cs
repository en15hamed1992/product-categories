using System.Security.Claims;
using Application.Handlers;
using Microsoft.AspNetCore.Http;

namespace Application.Services
{
    public abstract class BaseService:ResultHandler
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
      
        protected BaseService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
           
        }
        public string UserId => _httpContextAccessor.HttpContext?.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
    }
}