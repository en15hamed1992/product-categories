﻿#nullable enable
using Application.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace Application.Handlers
{
    public  class ResultHandler
    {
        protected IActionResult Ok(string? message,object? data=null)
            => new JsonResult(new BaseResult()
            {
            Success = true,
            Message = message??"", 
            Data=data??new {},
            Errors = new {}
            })
            {StatusCode = 200};

        protected IActionResult Create(string? message,object? data=null)
            => new JsonResult(new BaseResult()
                {
                    Success = true,
                    Message = message??"", 
                    Data=data??new {},
                    Errors = new {}
                })
                {StatusCode = 201};

        protected IActionResult BadRequest(string? message, object? errors=null)
            => new JsonResult(new BaseResult()
                {
                    Success = false,
                    Message = message??"", 
                    Data=new {},
                    Errors =errors?? new {}
                })
                {StatusCode = 400};

        protected IActionResult NotFound(string? message)
            => new JsonResult(new BaseResult()
                {
                    Success = false,
                    Message = message??"", 
                    Data=new {},
                    Errors = new {}
                })
                {StatusCode = 404};
        
      
    }
}