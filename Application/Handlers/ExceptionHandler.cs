﻿using System;
using Application.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace Application.Handlers
{
    public static class ExceptionHandler
    {
        public static IActionResult Handel(this Exception ex) =>new JsonResult(new BaseResult()
            {
                Success = false,
                Message = ex.Message, 
                Data=new {},
                Errors =ex
            })
            {StatusCode = 500};
    }
}
