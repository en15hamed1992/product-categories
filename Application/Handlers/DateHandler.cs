﻿using System;

namespace Application.Handlers
{
    public static class DateHandler
    {
        public  static  string ToDateTimeString(this DateTime date)=>
            date.ToString("yyyy-MM-ddTHH:mm:ss");
    }
}