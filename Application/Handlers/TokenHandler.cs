﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Domain.Models.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

namespace Application.Handlers
{
    public static class TokenHandler
    {
        private static string AuthKey => "ThisKeyShouldBeSecret";
        private static SigningCredentials SigningCredentials=> new SigningCredentials(
                new SymmetricSecurityKey(Encoding.ASCII.GetBytes(AuthKey)),
                SecurityAlgorithms.HmacSha512Signature);

        private static DateTime TokenExpiryDate => DateTime.UtcNow.AddHours(24);
        
        
        
        
        public static async Task<string> GenerateJwtToken(UserManager<UserEntity>userManager,UserEntity user)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(await userManager.GetClaimsAsync(user)),
                    Expires =  TokenExpiryDate,
                    SigningCredentials = SigningCredentials
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                return tokenHandler.WriteToken(token);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        public static async Task<string> GenerateRefreshToken(int len = 32)=>
            await Task.FromResult(GenerateToken(len)) ;
       
        
       
        private static string GenerateToken(int len)
        {
            var randomNumber = new byte[len];
            using var generator = RandomNumberGenerator.Create();
            generator.GetBytes(randomNumber);
            return Convert.ToBase64String(randomNumber);
        }
    }
}