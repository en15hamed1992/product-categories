﻿using System.Collections.Generic;

namespace Application.DTOs
{
    public class BaseResult
    {
        public bool Success { get; set; } 

        public string Message { get; set; } 

        public object Data { get; set; } 

        public object Errors { get; set; } 
        


    }
}
