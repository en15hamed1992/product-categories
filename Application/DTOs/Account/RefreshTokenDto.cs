﻿using System.ComponentModel.DataAnnotations;

namespace Application.DTOs.Account
{
    public class RefreshTokenDto
    {
        [Required]
        public string AccessToken { get; set; }
        
        [Required]
        public string RefreshToken { get; set; }

       
    }
}
