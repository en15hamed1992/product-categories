﻿using System.ComponentModel.DataAnnotations;

namespace Application.DTOs.Account
{
    public class SignUpDto
    {
        [Required]
        [StringLength(127)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(255)]
        public string LastName { get; set; }

        [Required]
        [StringLength(255)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [Phone]
        public string PhoneNumber { get; set; }  
        
        [Required]
        [StringLength(255, MinimumLength = 8)]
        [RegularExpression("(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&?]).*$",
            ErrorMessage = "Password Must Contain 1 Uppercase 1 Lowercase 1 Digit 1 Special Character")]
        public string Password { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 8)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

    }
}
