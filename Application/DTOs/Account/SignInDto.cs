﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Application.DTOs.Account
{
    public class SignInDto
    {
        [Required]
        [StringLength(255)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(255, MinimumLength =4)]
        public string Password { get; set; }

    }
}
