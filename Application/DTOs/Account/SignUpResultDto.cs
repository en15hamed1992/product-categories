﻿namespace Application.DTOs.Account
{
    public class SignUpResultDto
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string EmailOrPhone { get; set; }
    }
}
