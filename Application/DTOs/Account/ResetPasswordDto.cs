﻿using System.ComponentModel.DataAnnotations;

namespace Application.DTOs.Account
{
    public class ResetPasswordDto
    {
        [Required]
        [StringLength(255)]
        public string EmailOrPhone { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 8)]
        public string Password { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 8)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

    }
}
