﻿namespace Application.DTOs.Account
{
    public class TokenDto
    {
        public string RefreshToken { get; set; }

        public string AccessToken { get; set; }
    }
}
