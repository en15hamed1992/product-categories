﻿using System.ComponentModel.DataAnnotations;

namespace Application.DTOs.User
{
    public class AssignRoleDto
    {
        [Required]
        [StringLength(255)]
        public string RoleName { get; set; }
    }
}
