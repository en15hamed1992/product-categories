﻿using System.Collections.Generic;

namespace Application.DTOs.User
{
    public class UserDto : BaseUserDto
    {
        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string CreatedAt { get; set; }

        public ICollection<RoleDto> Roles { get; set; }
    }
}
