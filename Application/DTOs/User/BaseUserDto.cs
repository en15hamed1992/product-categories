﻿namespace Application.DTOs.User
{
    public abstract class BaseUserDto
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Rank { get; set; }
    }
}
