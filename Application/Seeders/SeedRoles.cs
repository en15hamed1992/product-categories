﻿using System;
using System.Threading.Tasks;
using Domain.Constants;
using Domain.Interfaces;
using Domain.Models.Account;
using Domain.Specifications.Role;

namespace Application.Seeders
{
    public static class SeedRoles
    {
        private static string AdminRoleId => "0f8fad5b-d9cb-469f-a165-70867728950e";
        private static string UserRoleId => "7c9e6679-7425-40de-944b-e07fc1f90ae7";

        public static async Task<bool> Seed(IRepository<RoleEntity,string> roleRepository,
            IUnitOfWork unitOfWork)
        {
            try
            {
                if (await roleRepository.FindAsync(new RoleSpecification(AdminRoleId)) is null)
                {
                    var adminRole = new RoleEntity()
                    {
                        Id = AdminRoleId,
                        Name = Roles.Admin.ToString(),
                        NormalizedName = Roles.Admin.ToString().ToUpper(),
                    };
                    roleRepository.Create(adminRole);
                    await unitOfWork.CompleteAsync();
                    Console.WriteLine($"Admin Role Has Been Created Successfully");
                }

                if (await roleRepository.FindAsync(new RoleSpecification(UserRoleId)) is null)
                {
                    var userRole = new RoleEntity()
                    {
                        Id = UserRoleId,
                        Name = Roles.User.ToString(),
                        NormalizedName = Roles.User.ToString().ToUpper(),
                    };
                    roleRepository.Create(userRole);
                    await unitOfWork.CompleteAsync();
                    Console.WriteLine($"User Role Has Been Created Successfully");
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        

        }
        
    }
}