﻿using System;
using System.Threading.Tasks;
using Domain.Constants;
using Domain.Interfaces;
using Domain.Models.Account;
using Domain.Specifications.Role;
using Domain.Specifications.User;
using Microsoft.AspNetCore.Identity;

namespace Application.Seeders
{
    public static class SeedUsers
    {
        private static string AdminId => "2b8212c3-1a03-48fe-ae9b-25c0df27cbe9";

        
        public static async Task<bool> Seed(UserManager<UserEntity>  userManager,IUnitOfWork unitOfWork)
        {
            try
            {
                if (await userManager.FindByIdAsync(AdminId) is null)
                {
                    var admin = new UserEntity()
                    {
                        Id = AdminId,
                        FirstName = "admin",
                        LastName = "admin",
                        Email = "admin@admin.com",
                        PhoneNumber = "123123123"
                    };
                    var createTask= userManager.CreateAsync(admin,"Admin1@");
                    var addToRoleTask= userManager.AddToRoleAsync(admin, Roles.Admin.ToString());
                    await Task.WhenAll(createTask, addToRoleTask);
                    await unitOfWork.CompleteAsync();
                    Console.WriteLine($"Admin Role Has Been Created Successfully");
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
           
          

        }
    }
}