﻿using System.Threading.Tasks;
using Domain.Interfaces;
using Domain.Models.Account;
using Microsoft.AspNetCore.Identity;

namespace Application.Seeders
{
    public static class Seeder
    {
        public static async void Seed(UserManager<UserEntity> userManager,
            IRepository<RoleEntity, string> roleRepository,
            IUnitOfWork unitOfWork)
        {

            var seedRolesTask = SeedRoles.Seed(roleRepository, unitOfWork);
            var seedUsersTask=   SeedUsers.Seed(userManager,unitOfWork);


           await Task.WhenAll(seedRolesTask, seedUsersTask);
        }
    }
}