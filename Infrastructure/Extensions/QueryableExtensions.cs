﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using Domain.Interfaces;

namespace Infrastructure.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<TResult> Project<T, TResult>(this IQueryable<T> query,
            IProjectSpecification<T, TResult> specification)
        {
            return query.Select(specification.Projection);
        }
        
        public static IQueryable<T> ApplyPaging<T>(this IQueryable<T> query, int page, byte pageSize)
        {
            if (pageSize <= 0)
            {
                pageSize = 10;
            }
            if (page <= 0)
            {
                page = 1;
            }

            return query.Skip((page - 1) * pageSize).Take(pageSize);
        }

        public static IQueryable<T> ApplySpecification<T>(this IQueryable<T> inputQuery, ISpecification<T> specification)
            where T : class
        {
            var query = inputQuery;

            // modify the IQueryable using the specification's criteria expression
            if (specification.Criteria != null)
            {
                query = query.Where(specification.Criteria);
            }

            // filter by all filter expressions
            query = specification.Filters.Aggregate(query,
                                    (current, filter) => current.Where(filter));

            // Includes all expression-based includes
            query = specification.Includes.Aggregate(query,
                                    (current, include) => current.Include(include));

            // Include any string-based include statements
            query = specification.IncludeStrings.Aggregate(query,
                                    (current, include) => current.Include(include));


            // Apply ordering if expressions are set
            if (specification.OrderBy != null)
            {
                var sortedQuery = query.OrderBy(specification.OrderBy);

                // Apply then ordering if expressions are set
                if (specification.ThenBy != null)
                {
                    sortedQuery = sortedQuery.ThenBy(specification.ThenBy);
                }
                if (specification.ThenByDescending != null)
                {
                    sortedQuery = sortedQuery.ThenBy(specification.ThenByDescending);
                }
                query = sortedQuery;
            }
            else if (specification.OrderByDescending != null)
            {
                var sortedQuery = query.OrderByDescending(specification.OrderByDescending);

                // Apply then ordering if expressions are set
                if (specification.ThenBy != null)
                {
                    sortedQuery = sortedQuery.ThenBy(specification.ThenBy);
                }
                if (specification.ThenByDescending != null)
                {
                    sortedQuery = sortedQuery.ThenBy(specification.ThenByDescending);
                }
                query = sortedQuery;
            }

            // Apply group by if expressions are set
            if (specification.GroupBy != null)
            {
                query = query.GroupBy(specification.GroupBy).SelectMany(x => x);
            }
            
            // Apply ignore query filters
            if (specification.IgnoreQueryFilters)
            {
                query = query.IgnoreQueryFilters();
            }
            
            return query;
        }

    }
}
