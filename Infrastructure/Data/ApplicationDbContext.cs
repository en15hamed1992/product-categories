﻿using Domain.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Domain.Models.Account;
using Infrastructure.Extensions;

namespace Infrastructure.Data
{
    public class ApplicationDbContext : IdentityDbContext<UserEntity, RoleEntity, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        
        }

        public override int SaveChanges()
        {


            List<Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry> modifiedEntities =
                    ChangeTracker.Entries().Where(e => e.State == EntityState.Modified).ToList();
            modifiedEntities.ForEach(modifiedEntity =>
            {
                Type entityType = modifiedEntity.Entity.GetType().GetInterface(nameof(IEntity<int>)) ??
                    modifiedEntity.Entity.GetType().GetInterface(nameof(IEntity<string>));

                if (entityType != null)
                    modifiedEntity.Property(nameof(BaseEntity.UpdatedAt)).CurrentValue = DateTime.UtcNow;
            });
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken=default)
        {

            List<Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry> modifiedEntities = ChangeTracker.Entries().Where(e => e.State == EntityState.Modified).ToList();
            modifiedEntities.ForEach(modifiedEntity =>
            {
                Type entityType = modifiedEntity.Entity.GetType().GetInterface(nameof(IEntity<int>)) ??
                     modifiedEntity.Entity.GetType().GetInterface(nameof(IEntity<string>));
                if (entityType != null)
                {
                    cancellationToken.ThrowIfCancellationRequested();
                    modifiedEntity.Property(nameof(BaseEntity.UpdatedAt)).CurrentValue = DateTime.UtcNow;

                }
            });
            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            builder.SeedRoles();
            builder.SeedUsers();
            builder.SeedUserRoles();

        }


       
    }
}